#syntax=docker/dockerfile:1.7-labs
FROM php:7.4-apache

WORKDIR /var/www/html

ARG COFFEE_VERSION=v1

COPY --exclude=**/*.png ./src .
COPY ./src/images/coffee-$COFFEE_VERSION.png ./images/coffee.png

RUN a2enmod rewrite

EXPOSE 80

ENTRYPOINT ["apache2-foreground"]